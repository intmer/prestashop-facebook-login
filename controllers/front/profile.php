<?php
/**
 * Intmer Facebook Login for Prestashop
 *
 * @author    Intmer
 * @copyright 2015 Intmer
 * @license   Commercialware
 */

class IntmerFacebookLoginProfileModuleFrontController extends ModuleFrontController
{
    public $display_column_left = false;
    public $auth = true;

    public function initContent()
    {
        parent::initContent();
    }

    /**
     * @see FrontControllerCore::postProcess()
     */
    public function postProcess()
    {
        $action = Tools::getValue('action');
        $ajax = Tools::getValue('ajax');

        if ($action && $ajax && method_exists($this, 'ajaxProcess'.Tools::toCamelCase($action))) {
            $this->{'ajaxProcess'.Tools::toCamelCase($action)}();
            exit();
        } elseif ($action && $ajax) {
            die(Tools::jsonEncode(array('success' => false, 'error' => 'Method doesn\'t exist')));
        }

        $customer_id = (int)$this->context->cookie->id_customer;
        $access_token = FacebookConnection::getTokenByCustomer($customer_id);

        if ($access_token) {
            $fb = $this->module->initFacebook();
            $fb->setDefaultAccessToken($access_token);

            if ($action == 'disconnect') {
                try {
                    $response = $fb->delete('/me/permissions');
                    $response = $response->getGraphObject();
                    if ($response['success']) {
                        FacebookConnection::deleteAllByCustomer($customer_id);
                        Tools::redirect($this->context->link->getPageLink('my-account', true));
                    }
                    exit();
                } catch (Facebook\Exceptions\FacebookResponseException $e) {
                    // When Graph returns an error
                    $this->context->smarty->assign(array(
                        'error' => 'Graph returned an error: '.$e->getMessage()
                    ));
                } catch (Facebook\Exceptions\FacebookSDKException $e) {
                    // When validation fails or other local issues
                    $this->context->smarty->assign(array(
                        'error' => 'Facebook SDK returned an error: '.$e->getMessage()
                    ));
                }
            }

            $is_logged = true;

            try {
                $response = $fb->get('/me');
                $userNode = $response->getGraphUser();
                $response = $fb->get('/me/picture?type=normal&redirect=false');
                $userAvatar = $response->getGraphObject();
            } catch (Facebook\Exceptions\FacebookResponseException $e) {
                // When Graph returns an error
                $this->context->smarty->assign(array(
                    'error' => 'Graph returned an error: '.$e->getMessage()
                ));
                $is_logged = false;
            } catch (Facebook\Exceptions\FacebookSDKException $e) {
                // When validation fails or other local issues
                $this->context->smarty->assign(array(
                    'error' => 'Facebook SDK returned an error: '.$e->getMessage()
                ));
                $is_logged = false;
            }

            if ($is_logged) {
                $this->context->smarty->assign(array(
                    'user_avatar' => $userAvatar['url'],
                    'user_id' => $userNode->getId(),
                    'user_name' => $userNode->getName(),
                    'user_email' => $userNode->getField('email'),
                    'user_gender' => $userNode->getField('gender'),
                    'user_link' => $userNode->getLink(),
                    'user_birthday_timestamp' => $userNode->getBirthday(),
                    'user_birthday' => $userNode->getBirthday() ? $userNode->getBirthday()->format('Y-m-d') : null,
                    'user_verified' => $userNode->getField('verified'),
                    'user_disconnect_link' => $this->context->link->getModuleLink('intmerfacebooklogin', 'profile', array('action' => 'disconnect'), true),
                ));
            }

            $this->context->smarty->assign(array(
                'is_logged' => $is_logged,
            ));

            $this->setTemplate('profile.tpl');
        } else {
            Tools::redirect($this->context->link->getPageLink('my-account', true));
        }
    }
}
