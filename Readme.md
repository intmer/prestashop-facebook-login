## Requirements

You need create a new facebook API Application to setup this plugin. Please follow the instructions provided below.

1. Go to [Facebook for Developers](https://developers.facebook.com/) and login with your Facebook account
2. Click on **MyApps** - **Add a new app** button (A popup will open)
3. Choose **Web-site**
4. Enter name of your app or site, click **Create new Facebook App ID** button.
5. Add the required information and don't forget to make your app live. This is very important otherwise your app will not work for all users.
6. Then click the **Create app** button and follow the instructions, your new app will be created.
7. Copy **App ID** and **App Secret**
8. Go to **Modules** - **Facebook login** - **Configure** - **Facebook application settings**, fill **App ID** and **App Secret** fields.

## System requirements

* PHP 5.4 or greater
* The mbstring extension
* The curl extension
