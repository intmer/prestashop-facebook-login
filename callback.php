<?php
/**
 * Intmer Facebook Login for Prestashop
 *
 * @author    Intmer
 * @copyright 2015 Intmer
 * @license   Commercialware
 */

if (!session_id()) {
    session_start();
}

include(dirname(__FILE__).'/../../config/config.inc.php');
include(dirname(__FILE__).'/../../init.php');

require_once dirname(__FILE__).'/intmerfacebooklogin.php';

function print_message($html, $class = 'alert alert-danger')
{
    $controller = new FrontController();
    $controller->init();
    $controller->setMedia();
    echo '<!DOCTYPE HTML>';
    ?>
    <html>
    <head>
        <meta charset="utf-8">
        <title></title>
        <?php
        if ($controller->css_files) {
            foreach ($controller->css_files as $url => $media) {
                ?>
                <link rel="stylesheet" href="<?php echo Tools::htmlentitiesUTF8($url); ?>" type="text/css" media="<?php echo Tools::htmlentitiesUTF8($media); ?>">
                <?php
            }
        }
        ?>
    </head>
    <body>
    <div id="page">
        <div class="columns-container">
            <div class="<?php echo $class; ?>"><?php echo $html; ?></div>
        </div>
    </div>
    </body>
    </html>
    <?php
}

$module = new IntmerFacebookLogin();

$fb = $module->initFacebook();

$helper = $fb->getRedirectLoginHelper();

$accessToken = null;

try {
    $accessToken = $helper->getAccessToken();

    $oAuth2Client = $fb->getOAuth2Client();
    $tokenMetadata = $oAuth2Client->debugToken($accessToken);
    $tokenMetadata->validateExpiration();
    if (!$accessToken->isLongLived()) {
        $accessToken = $oAuth2Client->getLongLivedAccessToken($accessToken);
    }
} catch (Facebook\Exceptions\FacebookResponseException $e) {
    // When Graph returns an error
    print_message('Graph returned an error: '.$e->getMessage());
    exit;
} catch (Facebook\Exceptions\FacebookSDKException $e) {
    // When validation fails or other local issues
    print_message('Facebook SDK returned an error: '.$e->getMessage());
    exit;
}

if ($accessToken) {
    // Logged in
    $fb->setDefaultAccessToken($accessToken);

    try {
        $response = $fb->get('/me');
        $userNode = $response->getGraphUser();
    } catch (Facebook\Exceptions\FacebookResponseException $e) {
        // When Graph returns an error
        print_message('Graph returned an error: '.$e->getMessage());
        exit;
    } catch (Facebook\Exceptions\FacebookSDKException $e) {
        // When validation fails or other local issues
        print_message('Facebook SDK returned an error: '.$e->getMessage());
        exit;
    }

    try {
        $success = $module->userAuthorization($accessToken, $userNode);
        if (!$success) {
            throw new PrestaShopException($module->getLastError());
        }
    } catch (PrestaShopException $e) {
        print_message($e->getMessage());
        exit;
    }
}

$method = Tools::getValue('method');
$opener = Tools::getValue('opener');
if ($module->isPopupWindowUsed() || ($method && $method == 'window')) {
    ?>
    <script>
        <?php if ($opener && $opener == 'reload'): ?>
        window.opener.location.reload();
        <?php else: ?>
        window.opener.location = '<?php echo _PS_BASE_URL_.__PS_BASE_URI__; ?>';
        <?php endif; ?>
        window.close();
    </script>
    <?php
} else {
    Tools::redirect(_PS_BASE_URL_.__PS_BASE_URI__);
}
