<?php
/**
 * Intmer Facebook Login for Prestashop
 *
 * @author    Intmer
 * @copyright 2015 Intmer
 * @license   Commercialware
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

$result = true;

$sql = array();

$sql[] = 'DROP TABLE IF EXISTS `'._DB_PREFIX_.self::TABLE_NAME.'`;';

foreach ($sql as $query) {
    $result = $result && Db::getInstance()->execute($query);
}

return $result;
