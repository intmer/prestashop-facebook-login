<?php
/**
 * Intmer Facebook Login for Prestashop
 *
 * @author    Intmer
 * @copyright 2015 Intmer
 * @license   Commercialware
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

$result = true;

$sql = array();

$sql[] = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.self::TABLE_NAME.'` (
    `id_connection` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
    `created` DATE NOT NULL,
    `id_customer` INT(11) UNSIGNED NOT NULL,
    `id_system` VARCHAR(20) NOT NULL,
    `access_token` VARCHAR(255) NOT NULL,
    PRIMARY KEY  (`id_connection`)
) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8;';

foreach ($sql as $query) {
    if (Db::getInstance()->execute($query) == false) {
        $result = $result && false;
        break;
    }
}

return $result;
