
[{shop_url}] 

Hi {name},

Your account successfully created.

Use following credential to login to your account:
Login: {email}
Password: {password}

Or use the Facebook login button for quick authorization.

Note: You can change your password in your personal account.

{shop_name} [{shop_url}] powered by
PrestaShop(tm) [http://www.prestashop.com/] 

