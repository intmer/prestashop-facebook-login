{*
* Intmer Facebook Login for Prestashop
*
* @author    Intmer
* @copyright 2015 Intmer
* @license   Commercialware
*}
{if !$is_ps_logged && !$is_logged}
    <!-- MODULE Intmer Facebook Login -->
    <div id="intmerfacebooklogin-link">
        <a href="{$login_url|escape:'htmlall':'UTF-8'}"{if $method == 'window'} onclick="window.open('{$login_url|escape:'htmlall':'UTF-8'}','intmerfacebooklogin','height=600,width=600,status=0,menubar=0,location=0,toolbar=0');return false;"{/if} rel="nofollow" title="">{l s='Login with Facebook' mod='intmerfacebooklogin'}</a>
    </div>
    <!-- /MODULE Intmer Facebook Login -->
{/if}
