{*
* Intmer Facebook Login for Prestashop
*
* @author    Intmer
* @copyright 2015 Intmer
* @license   Commercialware
*}
{capture name=path}
    <a href="{$link->getPageLink('my-account', true)|escape:'htmlall':'UTF-8'}">{l s='My account' mod='intmerfacebooklogin'}</a>
    <span class="navigation-pipe">{$navigationPipe|escape:'htmlall':'UTF-8'}</span>
    {l s='My facebook profile' mod='intmerfacebooklogin'}
{/capture}
<!-- MODULE Intmer Facebook Login -->
<div id="intmerfacebooklogin-profile">
    <h1 class="page-heading">{l s='My facebook profile' mod='intmerfacebooklogin'}</h1>
    {if $error}
        <div class="alert alert-danger" role="alert">{$error|escape:'htmlall':'UTF-8'}</div>
    {/if}
    {if $is_logged}
        <div class="form-horizontal">
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <a href="{$user_link|escape:'htmlall':'UTF-8'}" target="_blank" title="{l s='Go to facebook profile' mod='intmerfacebooklogin'}">
                        <img src="{$user_avatar|escape:'htmlall':'UTF-8'}" alt="{$user_name|escape:'htmlall':'UTF-8'}">
                    </a>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">{l s='ID' mod='intmerfacebooklogin'}</label>
                <div class="col-sm-10">
                    <p class="form-control-static">{$user_id|escape:'htmlall':'UTF-8'}</p>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">{l s='Name' mod='intmerfacebooklogin'}</label>
                <div class="col-sm-10">
                    <p class="form-control-static">{$user_name|escape:'htmlall':'UTF-8'}</p>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">{l s='Email' mod='intmerfacebooklogin'}</label>
                <div class="col-sm-10">
                    <p class="form-control-static">{$user_email|escape:'htmlall':'UTF-8'}</p>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">{l s='Gender' mod='intmerfacebooklogin'}</label>
                <div class="col-sm-10">
                    <p class="form-control-static">{$user_gender|capitalize|escape:'htmlall':'UTF-8'}</p>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">{l s='Birthday' mod='intmerfacebooklogin'}</label>
                <div class="col-sm-10">
                    <p class="form-control-static">{$user_birthday|escape:'htmlall':'UTF-8'}</p>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">{l s='Verified' mod='intmerfacebooklogin'}</label>
                <div class="col-sm-10">
                    <p class="form-control-static">
                        {if $user_verified}
                            {l s='Yes' mod='intmerfacebooklogin'}
                        {else}
                            {l s='No' mod='intmerfacebooklogin'}
                        {/if}
                    </p>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <a class="btn btn-default" href="{$user_link|escape:'htmlall':'UTF-8'}" target="_blank">{l s='Go to facebook profile' mod='intmerfacebooklogin'}</a>
                    <a class="btn btn-default" href="{$user_disconnect_link|escape:'html':'UTF-8'}">{l s='Disconnect' mod='intmerfacebooklogin'}</a>
                </div>
            </div>
        </div>
    {/if}
    <ul class="footer_links clearfix">
        <li>
            <a class="btn btn-default button button-small" href="{$link->getPageLink('my-account', true)|escape:'htmlall':'UTF-8'}" title="{l s='Back to Your Account' mod='intmerfacebooklogin'}">
                <span><i class="icon-chevron-left"></i> {l s='Back to Your Account' mod='intmerfacebooklogin'}</span>
            </a>
        </li>
        <li>
            <a class="btn btn-default button button-small" href="{if isset($force_ssl) && $force_ssl}{$base_dir_ssl|escape:'htmlall':'UTF-8'}{else}{$base_dir|escape:'htmlall':'UTF-8'}{/if}" title="{l s='Home' mod='intmerfacebooklogin'}">
                <span><i class="icon-chevron-left"></i> {l s='Home' mod='intmerfacebooklogin'}</span>
            </a>
        </li>
    </ul>
</div>
<!-- /MODULE Intmer Facebook Login -->
