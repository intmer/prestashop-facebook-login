{*
* Intmer Facebook Login for Prestashop
*
* @author    Intmer
* @copyright 2015 Intmer
* @license   Commercialware
*}
{if $is_logged}
    <!-- MODULE Intmer Facebook Login -->
    <div class="col-sm-1 clearfix"></div>
    <!-- /MODULE Intmer Facebook Login -->
{else}
    <!-- MODULE Intmer Facebook Login -->
    <div id="intmerfacebooklogin-top-link" class="col-sm-1 clearfix">
        <a href="{$login_url|escape:'htmlall':'UTF-8'}"{if $method == 'window'} onclick="window.open('{$login_url|escape:'htmlall':'UTF-8'}','intmerfacebooklogin','height=600,width=600,status=0,menubar=0,location=0,toolbar=0');return false;"{/if} rel="nofollow" class="btn btn-default" title="{l s='Login with Facebook' mod='intmerfacebooklogin'}"></a>
    </div>
    <!-- /MODULE Intmer Facebook Login -->
{/if}
