{*
* Intmer Facebook Login for Prestashop
*
* @author    Intmer
* @copyright 2015 Intmer
* @license   Commercialware
*}
<!-- MODULE Intmer Facebook Login -->
<li class="lnk_intmerfacebooklogin intmerfacebooklogin-account-link">
    {if $is_connected}
        <a href="{$link->getModuleLink('intmerfacebooklogin', 'profile', array(), true)|escape:'html':'UTF-8'}" title="{l s='Your account connected to facebook' mod='intmerfacebooklogin'}">
            <i class="icon-facebook"></i>
            <span>{l s='%s connected' mod='intmerfacebooklogin' sprintf=[$connection->id_system]}</span>
        </a>
    {else}
        <a href="{$login_url|escape:'htmlall':'UTF-8'}" onclick="window.open('{$login_url|escape:'htmlall':'UTF-8'}','intmerfacebooklogin','height=600,width=600,status=0,menubar=0,location=0,toolbar=0');return false;" title="{l s='Your account is not connected to facebook' mod='intmerfacebooklogin'}">
            <i class="icon-facebook"></i>
            <span>{l s='Connect to Facebook' mod='intmerfacebooklogin'}</span>
        </a>
    {/if}
</li>
<!-- /MODULE Intmer Facebook Login -->
