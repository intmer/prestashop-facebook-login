{*
* Intmer Facebook Login for Prestashop
*
* @author    Intmer
* @copyright 2015 Intmer
* @license   Commercialware
*}
<div class="panel">
    <h3><i class="icon icon-tags"></i> {l s='Documentation' mod='intmerfacebooklogin'}</h3>
    <ul>

    </ul>
    <p>If you're having trouble with Facebook Login module, make sure to check out the troubleshooting wiki page.</p>
</div>
