<?php
/**
 * Intmer Facebook Login for Prestashop
 *
 * @author    Intmer
 * @copyright 2015 Intmer
 * @license   Commercialware
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

class FacebookConnection extends ObjectModel
{
    public $id_connection;
    public $created;
    public $id_customer;
    public $id_system;
    public $access_token;

    public static $definition = array(
        'table' => IntmerFacebookLogin::TABLE_NAME,
        'primary' => 'id_connection',
        'multilang' => false,
        'fields' => array(
            'created' => array('required' => true, 'type' => self::TYPE_DATE),
            'id_customer' => array('required' => true, 'type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
            'id_system' => array('required' => true, 'type' => self::TYPE_STRING),
            'access_token' => array('required' => true, 'type' => self::TYPE_STRING),
        ),
    );

    public function __construct($id_connection = null, $id_lang = null, $id_shop = null)
    {
        parent::__construct($id_connection, $id_lang, $id_shop);
    }

    public static function findAll()
    {
        $sql = new DbQuery();
        $sql->select('*');
        $sql->from(IntmerFacebookLogin::TABLE_NAME, 'sbpt');
        $sql->orderBy('id_connection ASC');

        return Db::getInstance()->executeS($sql);
    }

    public static function findByCustomer($id_customer)
    {
        if (!Validate::isUnsignedInt($id_customer)) {
            return false;
        }

        $sql = new DbQuery();
        $sql->select('id_connection');
        $sql->from(IntmerFacebookLogin::TABLE_NAME, 'sbpt');
        $sql->where('id_customer = '.(int)$id_customer);

        if ($row = Db::getInstance()->getRow($sql)) {
            return new FacebookConnection($row['id_connection']);
        }

        return null;
    }

    /**
     * If customer has no token, it means that he is not connected to facebook
     *
     * @param $id_customer
     * @return null|string
     */
    public static function getTokenByCustomer($id_customer)
    {
        $connection = self::findByCustomer($id_customer);
        if ($connection) {
            return $connection->access_token;
        }

        return null;
    }

    public static function findBySystemId($id_system)
    {
        $sql = new DbQuery();
        $sql->select('id_connection');
        $sql->from(IntmerFacebookLogin::TABLE_NAME, 'sbpt');
        $sql->where('id_system = \''.Db::getInstance()->_escape($id_system).'\'');

        if ($row = Db::getInstance()->getRow($sql)) {
            return new FacebookConnection($row['id_connection']);
        }

        return null;
    }

    public static function find($id_customer, $id_system)
    {
        if (!Validate::isUnsignedInt($id_customer)) {
            return false;
        }

        $sql = new DbQuery();
        $sql->select('*');
        $sql->from(IntmerFacebookLogin::TABLE_NAME, 'sbpt');
        $sql->where('id_customer = '.(int)$id_customer.' AND id_system = \''.Db::getInstance()->_escape($id_system).'\'');

        return Db::getInstance()->getValue($sql);
    }

    public static function deleteAllByCustomer($id_customer)
    {
        if (!Validate::isUnsignedInt($id_customer)) {
            return false;
        }

        return Db::getInstance()->delete(IntmerFacebookLogin::TABLE_NAME, 'id_customer = '.(int)$id_customer);
    }

    public function save($null_values = false, $auto_date = true)
    {
        if ((int)$this->id == 0) {
            if ($auto_date) {
                $this->created = date('Y-m-d');
            }

            $id_system_exists = self::findBySystemId($this->id_system);
            if ($id_system_exists) {
                throw new PrestaShopException('This Facebook account is already connected to another account');
            }
        }

        return parent::save($null_values, $auto_date);
    }
}
