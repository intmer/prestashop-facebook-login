<?php
/**
 * Intmer Facebook Login for Prestashop
 *
 * @author    Intmer
 * @copyright 2015 Intmer
 * @license   Commercialware
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

require_once dirname(__FILE__).'/classes/AbstractAddon.php';

require_once dirname(__FILE__).'/models/FacebookConnection.php';

require_once dirname(__FILE__).'/classes/Facebook/autoload.php';

if (!session_id()) {
    session_start();
}

class IntmerFacebookLogin extends Module
{
    const PREFIX = 'INTMER_';
    const TABLE_PREFIX = 'intmer';
    const TABLE_NAME = 'intmerfacebooklogin';

    private $_real_path = null;

    private $_fb = null;
    private $_fb_login_url = null;

    protected $error = '';

    public function __construct()
    {
        $this->name = 'intmerfacebooklogin';
        $this->tab = 'front_office_features';
        $this->version = '1.0.0';
        $this->author = 'Intmer';
        $this->controllers = array('profile');
        $this->need_instance = 0;

        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Intmer Facebook Login');
        $this->description = $this->l('A simple way to trigger the facebook authentication process on your website');

        $this->confirmUninstall = $this->l('Are you sure?');

        $this->ps_versions_compliancy = array('min' => '1.6.1.0', 'max' => _PS_VERSION_);
    }

    public function getLastError()
    {
        return $this->error;
    }

    public function getRealPath()
    {
        if (is_null($this->_real_path)) {
            $this->_real_path = realpath(dirname(__FILE__)).'/';
        }

        return $this->_real_path;
    }

    public function getModuleBaseUri()
    {
        return Tools::getHttpHost(true)._MODULE_DIR_.$this->name.'/';
    }

    public function install()
    {
        $result = true;

        Configuration::updateValue(self::PREFIX.'FACEBOOKLOGIN_APP_ID', '');
        Configuration::updateValue(self::PREFIX.'FACEBOOKLOGIN_APP_SECRET', '');
        Configuration::updateValue(self::PREFIX.'FACEBOOKLOGIN_APP_PERMISSIONS_USER_BIRTHDAY', 0);
        Configuration::updateValue(self::PREFIX.'FACEBOOKLOGIN_APP_PERMISSIONS_USER_FRIENDS', 0);
        Configuration::updateValue(self::PREFIX.'FACEBOOKLOGIN_APP_PERMISSIONS_USER_LIKES', 0);
        Configuration::updateValue(self::PREFIX.'FACEBOOKLOGIN_APP_PERMISSIONS_USER_POSTS', 0);

        Configuration::updateValue(self::PREFIX.'FACEBOOKLOGIN_NEWSLETTER_SUBSCRIPTION', true);
        Configuration::updateValue(self::PREFIX.'FACEBOOKLOGIN_ADS_SUBSCRIPTION', true);
        $access_groups = Group::getGroups($this->context->language->id, true);
        $default_access_group = current($access_groups);
        Configuration::updateValue(self::PREFIX.'FACEBOOKLOGIN_GROUP_ACCESS', Tools::jsonEncode(array($default_access_group['id_group'])));
        Configuration::updateValue(self::PREFIX.'FACEBOOKLOGIN_AUTHORIZATION_METHOD', 'window');

        $result = $result && parent::install();

        $result = $result && require_once(dirname(__FILE__).'/init/install.php');

        $result = $result && $this->registerHook('header');
        $result = $result && $this->registerHook('backOfficeHeader');
        $result = $result && $this->registerHook('displayNav');
        $result = $result && $this->registerHook('actionCustomerLogoutAfter');
        $result = $result && $this->registerHook('actionObjectCustomerDeleteAfter');
        $result = $result && $this->registerHook('customerAccount');

        return $result;
    }

    public function uninstall()
    {
        Configuration::deleteByName(self::PREFIX.'FACEBOOKLOGIN_APP_ID');
        Configuration::deleteByName(self::PREFIX.'FACEBOOKLOGIN_APP_SECRET');
        Configuration::deleteByName(self::PREFIX.'FACEBOOKLOGIN_APP_PERMISSIONS_USER_BIRTHDAY');
        Configuration::deleteByName(self::PREFIX.'FACEBOOKLOGIN_APP_PERMISSIONS_USER_FRIENDS');
        Configuration::deleteByName(self::PREFIX.'FACEBOOKLOGIN_APP_PERMISSIONS_USER_LIKES');
        Configuration::deleteByName(self::PREFIX.'FACEBOOKLOGIN_APP_PERMISSIONS_USER_POSTS');

        Configuration::deleteByName(self::PREFIX.'FACEBOOKLOGIN_NEWSLETTER_SUBSCRIPTION');
        Configuration::deleteByName(self::PREFIX.'FACEBOOKLOGIN_ADS_SUBSCRIPTION');
        Configuration::deleteByName(self::PREFIX.'FACEBOOKLOGIN_GROUP_ACCESS');
        Configuration::deleteByName(self::PREFIX.'FACEBOOKLOGIN_AUTHORIZATION_METHOD');

        require_once(dirname(__FILE__).'/init/uninstall.php');

        return parent::uninstall();
    }

    public function getContent()
    {
        $submit = false;

        if (((bool)Tools::isSubmit('submitApplicationModule')) == true) {
            $submit = true;

            $this->processApplicationForm();
        }

        if (((bool)Tools::isSubmit('submitSettingsModule')) == true) {
            $submit = true;

            $this->processSettingsForm();
        }

        $this->context->smarty->assign('module_dir', $this->_path);

        $output = '';

        if ($submit) {
            $output .= $this->display(__FILE__, 'views/templates/admin/messages.tpl');
        }

        if ($this->isMisconfiguration()) {
            $output .= $this->display(__FILE__, 'views/templates/admin/getting-started.tpl');
        }

        return $output.$this->renderApplicationForm().$this->renderSettingsForm().$this->display(__FILE__, 'views/templates/admin/documentation.tpl');
    }

    protected function renderApplicationForm()
    {
        $helper = new HelperForm();

        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $helper->module = $this;
        $helper->default_form_language = $this->context->language->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG', 0);

        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitApplicationModule';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false)
            .'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');

        $helper->tpl_vars = array(
            'fields_value' => $this->getApplicationFormValues(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
        );

        return $helper->generateForm(array($this->getApplicationForm()));
    }

    protected function getApplicationForm()
    {
        return array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Facebook Application Settings'),
                    'icon' => 'icon-cogs',
                ),
                'input' => array(
                    array(
                        'col' => 2,
                        'type' => 'text',
                        //'desc' => '',
                        'name' => self::PREFIX.'FACEBOOKLOGIN_APP_ID',
                        'label' => $this->l('App ID'),
                    ),
                    array(
                        'col' => 3,
                        'type' => 'text',
                        //'desc' => '',
                        'name' => self::PREFIX.'FACEBOOKLOGIN_APP_SECRET',
                        'label' => $this->l('App Secret'),
                    ),
                    array(
                        'type' => 'label',
                        'label' => $this->l('Permissions').':',
                    ),
                    array(
                        'type' => 'label',
                        'desc' => $this->l('Public profile and email will be requested by default'),
                        'label' => '',
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $this->l('User birthday'),
                        'name' => self::PREFIX.'FACEBOOKLOGIN_APP_PERMISSIONS_USER_BIRTHDAY',
                        'default' => 0,
                        'is_bool' => true,
                        'desc' => $this->l('Access the date and month of a person\'s birthday').' ('.$this->l('Facebook will have to review how your app uses it').')',
                        'values' => array(
                            array(
                                'value' => 1,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'value' => 0,
                                'label' => $this->l('Disabled')
                            )
                        ),
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $this->l('User friends'),
                        'name' => self::PREFIX.'FACEBOOKLOGIN_APP_PERMISSIONS_USER_FRIENDS',
                        'default' => 0,
                        'is_bool' => true,
                        'desc' => $this->l('Provides access the list of friends'),
                        'values' => array(
                            array(
                                'value' => 1,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'value' => 0,
                                'label' => $this->l('Disabled')
                            )
                        ),
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $this->l('User likes'),
                        'name' => self::PREFIX.'FACEBOOKLOGIN_APP_PERMISSIONS_USER_LIKES',
                        'default' => 0,
                        'is_bool' => true,
                        'desc' => $this->l('Provides access to the list of all Facebook Pages and Open Graph objects that a person has liked').' ('.$this->l('Facebook will have to review how your app uses it').')',
                        'values' => array(
                            array(
                                'value' => 1,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'value' => 0,
                                'label' => $this->l('Disabled')
                            )
                        ),
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $this->l('User posts'),
                        'name' => self::PREFIX.'FACEBOOKLOGIN_APP_PERMISSIONS_USER_POSTS',
                        'default' => 0,
                        'is_bool' => true,
                        'desc' => $this->l('Provides access to the posts on a person\'s Timeline').' ('.$this->l('Facebook will have to review how your app uses it').')',
                        'values' => array(
                            array(
                                'value' => 1,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'value' => 0,
                                'label' => $this->l('Disabled')
                            )
                        ),
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                ),
            ),
        );
    }

    protected function getApplicationFormValues()
    {
        $values = array(
            self::PREFIX.'FACEBOOKLOGIN_APP_ID' => Configuration::get(self::PREFIX.'FACEBOOKLOGIN_APP_ID'),
            self::PREFIX.'FACEBOOKLOGIN_APP_SECRET' => Configuration::get(self::PREFIX.'FACEBOOKLOGIN_APP_SECRET'),
            self::PREFIX.'FACEBOOKLOGIN_APP_PERMISSIONS_USER_BIRTHDAY' => Configuration::get(self::PREFIX.'FACEBOOKLOGIN_APP_PERMISSIONS_USER_BIRTHDAY'),
            self::PREFIX.'FACEBOOKLOGIN_APP_PERMISSIONS_USER_FRIENDS' => Configuration::get(self::PREFIX.'FACEBOOKLOGIN_APP_PERMISSIONS_USER_FRIENDS'),
            self::PREFIX.'FACEBOOKLOGIN_APP_PERMISSIONS_USER_LIKES' => Configuration::get(self::PREFIX.'FACEBOOKLOGIN_APP_PERMISSIONS_USER_LIKES'),
            self::PREFIX.'FACEBOOKLOGIN_APP_PERMISSIONS_USER_POSTS' => Configuration::get(self::PREFIX.'FACEBOOKLOGIN_APP_PERMISSIONS_USER_POSTS'),
        );

        $values[self::PREFIX.'FACEBOOKLOGIN_APP_PERMISSIONS'] = array('email');
        if ($values[self::PREFIX.'FACEBOOKLOGIN_APP_PERMISSIONS_USER_BIRTHDAY']) {
            $values[self::PREFIX.'FACEBOOKLOGIN_APP_PERMISSIONS'][] = 'user_birthday';
        }
        if ($values[self::PREFIX.'FACEBOOKLOGIN_APP_PERMISSIONS_USER_FRIENDS']) {
            $values[self::PREFIX.'FACEBOOKLOGIN_APP_PERMISSIONS'][] = 'user_friends';
        }
        if ($values[self::PREFIX.'FACEBOOKLOGIN_APP_PERMISSIONS_USER_LIKES']) {
            $values[self::PREFIX.'FACEBOOKLOGIN_APP_PERMISSIONS'][] = 'user_likes';
        }
        if ($values[self::PREFIX.'FACEBOOKLOGIN_APP_PERMISSIONS_USER_POSTS']) {
            $values[self::PREFIX.'FACEBOOKLOGIN_APP_PERMISSIONS'][] = 'user_posts';
        }

        return $values;
    }

    protected function processApplicationForm()
    {
        $form_values = $this->getApplicationFormValues();

        foreach (array_keys($form_values) as $key) {
            Configuration::updateValue($key, Tools::getValue($key));
        }
    }

    protected function renderSettingsForm()
    {
        $helper = new HelperForm();

        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $helper->module = $this;
        $helper->default_form_language = $this->context->language->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG', 0);

        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitSettingsModule';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false)
            .'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');

        $helper->tpl_vars = array(
            'fields_value' => $this->getSettingsFormValues(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
        );

        return $helper->generateForm(array($this->getSettingsForm()));
    }

    protected function getSettingsForm()
    {
        $access_groups = Group::getGroups($this->context->language->id, true);

        return array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Settings'),
                    'icon' => 'icon-cogs',
                ),
                'input' => array(
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Newsletter subscription for a new customer'),
                        'name' => self::PREFIX.'FACEBOOKLOGIN_NEWSLETTER_SUBSCRIPTION',
                        'default' => 1,
                        'is_bool' => true,
                        'desc' => $this->l('New customer will receive your newsletter via email'),
                        'values' => array(
                            array(
                                'value' => 1,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'value' => 0,
                                'label' => $this->l('Disabled')
                            )
                        ),
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Ads subscription for a new customer'),
                        'name' => self::PREFIX.'FACEBOOKLOGIN_ADS_SUBSCRIPTION',
                        'default' => 1,
                        'is_bool' => true,
                        'desc' => $this->l('New customer will receive your ads via email'),
                        'values' => array(
                            array(
                                'value' => 1,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'value' => 0,
                                'label' => $this->l('Disabled')
                            )
                        ),
                    ),
                    array(
                        'type' => 'group',
                        'label' => $this->l('Group access for a new customer'),
                        'name' => self::PREFIX.'FACEBOOKLOGIN_GROUP_ACCESS',
                        'values' => $access_groups,
                        'required' => true,
                        'col' => '6',
                        'desc' => $this->l('Sets the access group for a new customer'),
                    ),
                    array(
                        'type' => 'select',
                        'label' => $this->l('Authentication method'),
                        'name' => self::PREFIX.'FACEBOOKLOGIN_AUTHORIZATION_METHOD',
                        'default' => '',
                        'options' => array(
                            'query' => array(
                                '' => array(
                                    'value' => '',
                                    'name' => $this->l('Redirect'),
                                ),
                                'window' => array(
                                    'value' => 'window',
                                    'name' => $this->l('Window'),
                                ),
                            ),
                            'id' => 'value',
                            'name' => 'name'
                        )
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                ),
            ),
        );
    }

    protected function getSettingsFormValues()
    {
        $values = array(
            self::PREFIX.'FACEBOOKLOGIN_NEWSLETTER_SUBSCRIPTION' => Configuration::get(self::PREFIX.'FACEBOOKLOGIN_NEWSLETTER_SUBSCRIPTION'),
            self::PREFIX.'FACEBOOKLOGIN_ADS_SUBSCRIPTION' => Configuration::get(self::PREFIX.'FACEBOOKLOGIN_ADS_SUBSCRIPTION'),
            self::PREFIX.'FACEBOOKLOGIN_GROUP_ACCESS' => Tools::jsonDecode(Configuration::get(self::PREFIX.'FACEBOOKLOGIN_GROUP_ACCESS')),
            self::PREFIX.'FACEBOOKLOGIN_AUTHORIZATION_METHOD' => Configuration::get(self::PREFIX.'FACEBOOKLOGIN_AUTHORIZATION_METHOD'),
        );

        // Fix for groupbox type field
        foreach ($values[self::PREFIX.'FACEBOOKLOGIN_GROUP_ACCESS'] as $item) {
            $values['groupBox_'.$item] = true;
        }

        return $values;
    }

    protected function processSettingsForm()
    {
        $form_values = $this->getSettingsFormValues();

        foreach (array_keys($form_values) as $key) {
            Configuration::updateValue($key, Tools::getValue($key));
        }

        // Fix for groupbox type field
        $value = (array)Tools::getValue('groupBox');
        if ($value && $value[0] !== false) {
            Configuration::updateValue(self::PREFIX.'FACEBOOKLOGIN_GROUP_ACCESS', Tools::jsonEncode($value));
        }
    }

    protected function isMisconfiguration()
    {
        $app_settings = $this->getApplicationFormValues();

        return (empty($app_settings[self::PREFIX.'FACEBOOKLOGIN_APP_ID']) || empty($app_settings[self::PREFIX.'FACEBOOKLOGIN_APP_SECRET']));
    }

    public function isPopupWindowUsed()
    {
        $settings = $this->getSettingsFormValues();

        return $settings[self::PREFIX.'FACEBOOKLOGIN_AUTHORIZATION_METHOD'] != 'no';
    }

    public function initFacebook()
    {
        if (is_null($this->_fb)) {
            $app_settings = $this->getApplicationFormValues();

            $this->_fb = new Facebook\Facebook([
                'app_id' => $app_settings[self::PREFIX.'FACEBOOKLOGIN_APP_ID'],
                'app_secret' => $app_settings[self::PREFIX.'FACEBOOKLOGIN_APP_SECRET'],
                'default_graph_version' => 'v2.2',
            ]);
        }

        return $this->_fb;
    }

    public function getLoginUrl($permissions = array('email'), $callback_params = array())
    {
        if (is_null($this->_fb_login_url)) {
            $fb = $this->initFacebook();
            $helper = $fb->getRedirectLoginHelper();
            $callback_params_str = '';
            if ($callback_params) {
                foreach ($callback_params as $key => $value) {
                    $callback_params_str .= $callback_params_str ? '&' : '?';
                    $callback_params_str .= "{$key}={$value}";
                }
            }
            $this->_fb_login_url = $helper->getLoginUrl($this->getModuleBaseUri().'callback.php'.$callback_params_str, $permissions);
        }

        return $this->_fb_login_url;
    }

    public function userLogin($customer)
    {
        if (!Validate::isLoadedObject($customer)) {
            $this->error = Tools::displayError($this->l('Failed to load customer object, please contact us.'));

            return false;
        } elseif (isset($customer->active) && !$customer->active) {
            $this->error = Tools::displayError($this->l('Your account isn\'t available at this time, please contact us.'));

            return false;
        } elseif (!$customer || !$customer->id) {
            $this->error = Tools::displayError($this->l('Authentication failed, customer not found.'));

            return false;
        } else {
            $this->context->cookie->id_compare = isset($this->context->cookie->id_compare) ? $this->context->cookie->id_compare : CompareProduct::getIdCompareByIdCustomer($customer->id);
            $this->context->cookie->id_customer = (int)$customer->id;
            $this->context->cookie->customer_lastname = $customer->lastname;
            $this->context->cookie->customer_firstname = $customer->firstname;
            $this->context->cookie->logged = 1;
            $customer->logged = 1;
            $this->context->cookie->is_guest = $customer->isGuest();
            $this->context->cookie->passwd = $customer->passwd;
            $this->context->cookie->email = $customer->email;

            // Add customer to the context
            $this->context->customer = $customer;

            if (Configuration::get('PS_CART_FOLLOWING') && (empty($this->context->cookie->id_cart) || Cart::getNbProducts($this->context->cookie->id_cart) == 0) && $id_cart = (int)Cart::lastNoneOrderedCart($this->context->customer->id)) {
                $this->context->cart = new Cart($id_cart);
            } else {
                $id_carrier = (int)$this->context->cart->id_carrier;
                $this->context->cart->id_carrier = 0;
                $this->context->cart->setDeliveryOption(null);
                $this->context->cart->id_address_delivery = (int)Address::getFirstCustomerAddressId((int)($customer->id));
                $this->context->cart->id_address_invoice = (int)Address::getFirstCustomerAddressId((int)($customer->id));
            }
            $this->context->cart->id_customer = (int)$customer->id;
            $this->context->cart->secure_key = $customer->secure_key;

            $this->context->cart->save();
            $this->context->cookie->id_cart = (int)$this->context->cart->id;
            $this->context->cookie->write();
            $this->context->cart->autosetProductAddress();

            Hook::exec('actionAuthentication');

            // Login information have changed, so we check if the cart rules still apply
            CartRule::autoRemoveFromCart($this->context);
            CartRule::autoAddToCart($this->context);
        }

        return true;
    }

    public function userAuthorization($access_token, $userNode)
    {
        /*
         * @var CustomerCore $customer
         */

        //$id_shop = (int)Shop::getContextShopID();

        if ($userNode->getField('verified') !== true) {
            $this->error = Tools::displayError($this->l('Authentication failed. You should verify your facebook account first.'));

            return false;
        }

        Hook::exec('actionBeforeAuthentication');

        $settings = $this->getSettingsFormValues();

        $system_id = $userNode->getId();
        $connection = FacebookConnection::findBySystemId($system_id);

        if ($this->context->customer->isLogged()) {
            $customer_id = (int)$this->context->cookie->id_customer;
            $customer = new Customer($customer_id);
            $connection = new FacebookConnection();
            $connection_exists = $connection->find($customer->id, $userNode->getId());
            if (!$connection_exists) {
                $connection::deleteAllByCustomer($customer->id);
                $connection->id_customer = $customer->id;
                $connection->id_system = $userNode->getId();
                $connection->access_token = $access_token;
                $result = $connection->save();
                if (!$result) {
                    $this->error = Tools::displayError($this->l('Failed to connect customer account.'));

                    return false;
                }
            }

            return true;
        } else {
            if ($connection) {
                $customer = new Customer((int)$connection->id_customer);

                return $this->userLogin($customer);
            } else {
                $email = $userNode->getField('email');

                if (!Customer::customerExists($email)) {
                    $customer = new Customer();
                    $customer->active = 1;
                    $customer->email = $userNode->getField('email');
                    $customer->id_gender = $userNode->getField('gender') == 'male' ? 1 : 2;
                    $customer->firstname = $userNode->getFirstName();
                    $customer->lastname = $userNode->getLastName();
                    $customer->birthday = $userNode->getBirthday() ? $userNode->getBirthday()->format('Y-m-d') : null;
                    $customer->website = $userNode->getLink();
                    $customer->newsletter = $settings[self::PREFIX.'FACEBOOKLOGIN_NEWSLETTER_SUBSCRIPTION'];
                    if ($customer->newsletter) {
                        $customer->ip_registration_newsletter = Tools::getRemoteAddr();
                    }
                    $customer->optin = $settings[self::PREFIX.'FACEBOOKLOGIN_ADS_SUBSCRIPTION'];
                    $password = Tools::passwdGen(8);
                    $customer->passwd = Tools::encrypt($password);
                    $customer->groupBox = $settings[self::PREFIX.'FACEBOOKLOGIN_GROUP_ACCESS'];
                    if (!$customer->add()) {
                        $this->error = Tools::displayError($this->l('Authentication failed, failed to create account.'));

                        return false;
                    }

                    $connection = new FacebookConnection();
                    $connection_exists = $connection->find($customer->id, $userNode->getId());
                    if (!$connection_exists) {
                        $connection::deleteAllByCustomer($customer->id);
                        $connection->id_customer = $customer->id;
                        $connection->id_system = $userNode->getId();
                        $connection->access_token = $access_token;
                        $result = $connection->save();
                        if (!$result) {
                            $this->error = Tools::displayError($this->l('Failed to connect customer account.'));

                            return false;
                        }
                    }

                    $this->onAccountCreate($customer->email, $userNode->getName(), array(
                        '{email}' => $customer->email,
                        '{name}' => $userNode->getName(),
                        '{password}' => $password
                    ));

                    return $this->userLogin($customer);
                } else {
                    $this->error = Tools::displayError($this->l('User with this email address is already registered.'));

                    return false;
                }
            }
        }

        return true;
    }

    public function hookActionCustomerLogoutAfter()
    {

    }

    public function hookActionObjectCustomerDeleteAfter($params)
    {
        FacebookConnection::deleteAllByCustomer($params->object->id);
    }

    public function hookBackOfficeHeader()
    {
        //
    }

    public function hookHeader()
    {
        $this->context->controller->addCSS($this->_path.'/views/css/front.css');
    }

    public function hookCustomerAccount($params)
    {
        $params;

        $app_settings = $this->getApplicationFormValues();

        $connection = FacebookConnection::findByCustomer($this->context->cookie->id_customer);
        $is_connected = (bool)$connection;

        if (!$is_connected) {
            $login_url = $this->getLoginUrl($app_settings[self::PREFIX.'FACEBOOKLOGIN_APP_PERMISSIONS'], array(
                'method' => 'window',
                'opener' => 'reload'
            ));

            $this->smarty->assign(array(
                'login_url' => $login_url,
            ));
        }

        $this->smarty->assign(array(
            'is_connected' => $is_connected,
            'connection' => $connection,
        ));

        return $this->display(__FILE__, 'my-account.tpl');
    }

    public function actionFrontEnd($params)
    {
        $app_settings = $this->getApplicationFormValues();
        $settings = $this->getSettingsFormValues();

        $is_misconfiguration = $this->isMisconfiguration();

        if (!$is_misconfiguration) {
            $customer_id = (int)$this->context->cookie->id_customer;
            $access_token = FacebookConnection::getTokenByCustomer($customer_id);

            $is_logged = false;

            if ($access_token) {
                $fb = $this->initFacebook();

                $is_logged = true;

                $fb->setDefaultAccessToken($access_token);

                try {
                    $response = $fb->get('/me');
                    $userNode = $response->getGraphUser();
                } catch (Facebook\Exceptions\FacebookResponseException $e) {
                    // When Graph returns an error
                    //echo 'Graph returned an error: ' . $e->getMessage();
                    $is_logged = false;
                } catch (Facebook\Exceptions\FacebookSDKException $e) {
                    // When validation fails or other local issues
                    //echo 'Facebook SDK returned an error: ' . $e->getMessage();
                    $is_logged = false;
                }

                if ($is_logged) {
                    $this->smarty->assign(array(
                        'user_id' => $userNode->getId(),
                        'user_name' => $userNode->getName(),
                        'user_email' => $userNode->getField('email'),
                        'user_link' => $userNode->getLink(),
                        'user_birthday_timestamp' => $userNode->getBirthday(),
                    ));
                }
            }
            if (!$is_logged) {
                $login_url = $this->getLoginUrl($app_settings[self::PREFIX.'FACEBOOKLOGIN_APP_PERMISSIONS']);

                $this->smarty->assign(array(
                    'login_url' => $login_url,
                ));
            }

            $this->smarty->assign(array(
                'is_ps_logged' => $this->context->customer->isLogged(),
                'is_logged' => $is_logged,
                'method' => $settings[self::PREFIX.'FACEBOOKLOGIN_AUTHORIZATION_METHOD'],
            ));

            return $this->display(__FILE__, $params['hook'].'.tpl');
        }

        return '';
    }

    public function hookDisplayNav($params)
    {
        $params['hook'] = 'nav';

        return $this->actionFrontEnd($params);
    }

    public function hookDisplayTop($params)
    {
        $params['hook'] = 'top';

        return $this->actionFrontEnd($params);
    }

    protected function onAccountCreate($email, $name, $params = array())
    {
        return Mail::Send($this->context->language->id, 'account_created', Mail::l('Your account created', $this->context->language->id), $params, $email, $name, null, null, null, null, dirname(__FILE__).'/mails/', false, $this->context->shop->id);
    }
}
